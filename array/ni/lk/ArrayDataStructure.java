package array.ni.lk;

public class ArrayDataStructure {
	public static void demo() {
	
			int[] num = new int[5];
			num[0] = 20;
			num[1] = 10;
			num[2] = 50;
			num[3] = 20;
			num[4] = 60;

			int[] disNum = { 50, 55, 66, 70 };
			for (int i : disNum) {
				System.out.print(i + " , ");

			}

		}
	}
